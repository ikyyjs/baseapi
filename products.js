const products = [
    {
        "id": 1,
        "title": "Cool Gadgets",
        "subtitle": "Smart Living Solutions",
        "category": "Electronics",
        "price": 19.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 2,
        "title": "Adventure Gear",
        "subtitle": "Explore the Outdoors",
        "category": "Sports & Outdoors",
        "price": 24.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 3,
        "title": "Healthy Recipes",
        "subtitle": "Nutritious and Delicious Meals",
        "category": "Food & Cooking",
        "price": 15.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 4,
        "title": "Artistic Creations",
        "subtitle": "Express Your Imagination",
        "category": "Arts & Crafts",
        "price": 29.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 5,
        "title": "Fashion Trends",
        "subtitle": "Stylish Apparel for Every Occasion",
        "category": "Fashion",
        "price": 39.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 6,
        "title": "Home Decor Essentials",
        "subtitle": "Create a Cozy Living Space",
        "category": "Home & Living",
        "price": 49.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 7,
        "title": "Tech Innovations",
        "subtitle": "Stay Connected with the Latest Tech",
        "category": "Electronics",
        "price": 79.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 8,
        "title": "Fitness Essentials",
        "subtitle": "Achieve Your Health Goals",
        "category": "Sports & Outdoors",
        "price": 34.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 9,
        "title": "Culinary Adventures",
        "subtitle": "Master the Art of Cooking",
        "category": "Food & Cooking",
        "price": 27.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 10,
        "title": "Handmade Crafts",
        "subtitle": "Unique and Artistic Creations",
        "category": "Arts & Crafts",
        "price": 44.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 11,
        "title": "Smart Home Devices",
        "subtitle": "Enhance Your Living Space",
        "category": "Electronics",
        "price": 59.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 12,
        "title": "Outdoor Exploration Kit",
        "subtitle": "Discover Nature's Wonders",
        "category": "Sports & Outdoors",
        "price": 49.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 13,
        "title": "Vegetarian Delights Cookbook",
        "subtitle": "Delicious Plant-Based Recipes",
        "category": "Food & Cooking",
        "price": 18.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 14,
        "title": "Abstract Art Prints",
        "subtitle": "Expressive and Thought-Provoking",
        "category": "Arts & Crafts",
        "price": 34.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 15,
        "title": "Chic Wardrobe Essentials",
        "subtitle": "Fashionable Apparel for Every Season",
        "category": "Fashion",
        "price": 49.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 16,
        "title": "Cozy Bedding Collection",
        "subtitle": "Transform Your Bedroom",
        "category": "Home & Living",
        "price": 64.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 17,
        "title": "Smart Kitchen Gadgets",
        "subtitle": "Efficient and Innovative",
        "category": "Electronics",
        "price": 42.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 18,
        "title": "High-Intensity Workout Gear",
        "subtitle": "Achieve Peak Fitness",
        "category": "Sports & Outdoors",
        "price": 29.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 19,
        "title": "World Cuisine Cooking Class",
        "subtitle": "Explore Global Flavors",
        "category": "Food & Cooking",
        "price": 39.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 20,
        "title": "Handcrafted Jewelry",
        "subtitle": "Elegant and Unique Designs",
        "category": "Arts & Crafts",
        "price": 54.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 21,
        "title": "Smart Entertainment System",
        "subtitle": "Immersive Home Theater Experience",
        "category": "Electronics",
        "price": 79.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 22,
        "title": "Camping Essentials Bundle",
        "subtitle": "Everything You Need for a Great Trip",
        "category": "Sports & Outdoors",
        "price": 89.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 23,
        "title": "Mediterranean Cuisine Cookbook",
        "subtitle": "Delightful Flavors from the Region",
        "category": "Food & Cooking",
        "price": 24.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 24,
        "title": "Modern Abstract Sculptures",
        "subtitle": "Contemporary Art for Your Space",
        "category": "Arts & Crafts",
        "price": 59.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 25,
        "title": "Vintage Fashion Collection",
        "subtitle": "Timeless Styles for Every Wardrobe",
        "category": "Fashion",
        "price": 69.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 26,
        "title": "Luxurious Bedding Set",
        "subtitle": "Upgrade Your Sleeping Experience",
        "category": "Home & Living",
        "price": 89.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 27,
        "title": "Smart Office Gadgets",
        "subtitle": "Enhance Your Workspace",
        "category": "Electronics",
        "price": 49.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 28,
        "title": "Yoga and Pilates Equipment Set",
        "subtitle": "Achieve Mind-Body Balance",
        "category": "Sports & Outdoors",
        "price": 79.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 29,
        "title": "International Street Food Cookbook",
        "subtitle": "Savor Delicious Global Street Eats",
        "category": "Food & Cooking",
        "price": 32.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 30,
        "title": "Hand-Painted Ceramic Pottery",
        "subtitle": "Artistic and Functional Home Decor",
        "category": "Arts & Crafts",
        "price": 44.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 31,
        "title": "Home Theater Sound System",
        "subtitle": "Immersive Audio Experience",
        "category": "Electronics",
        "price": 129.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 32,
        "title": "Backpacking Adventure Kit",
        "subtitle": "Explore the Great Outdoors",
        "category": "Sports & Outdoors",
        "price": 99.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 33,
        "title": "Vegetarian Desserts Cookbook",
        "subtitle": "Sweet Delights without Meat",
        "category": "Food & Cooking",
        "price": 21.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 34,
        "title": "Contemporary Metal Sculptures",
        "subtitle": "Artistic Statements for Your Space",
        "category": "Arts & Crafts",
        "price": 79.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 35,
        "title": "Vintage Inspired Fashion",
        "subtitle": "Timeless Elegance in Every Outfit",
        "category": "Fashion",
        "price": 59.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 36,
        "title": "Luxury Bedroom Furniture Set",
        "subtitle": "Transform Your Sleep Sanctuary",
        "category": "Home & Living",
        "price": 249.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 37,
        "title": "Office Productivity Bundle",
        "subtitle": "Efficient Tools for Success",
        "category": "Electronics",
        "price": 89.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 38,
        "title": "Outdoor Fitness Equipment Set",
        "subtitle": "Achieve Active Lifestyle Goals",
        "category": "Sports & Outdoors",
        "price": 119.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 39,
        "title": "Global Street Food Adventure Cookbook",
        "subtitle": "Explore Diverse Culinary Delights",
        "category": "Food & Cooking",
        "price": 39.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 40,
        "title": "Handcrafted Ceramic Tableware",
        "subtitle": "Functional Art for Your Dining Table",
        "category": "Arts & Crafts",
        "price": 54.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 41,
        "title": "4K Ultra HD Smart TV",
        "subtitle": "Cinematic Entertainment at Home",
        "category": "Electronics",
        "price": 599.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 42,
        "title": "Wilderness Survival Gear",
        "subtitle": "Prepare for Outdoor Adventures",
        "category": "Sports & Outdoors",
        "price": 129.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 43,
        "title": "Vegan Baking Cookbook",
        "subtitle": "Delicious Plant-Based Desserts",
        "category": "Food & Cooking",
        "price": 29.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 44,
        "title": "Sculptural Wall Art",
        "subtitle": "Elevate Your Wall Decor",
        "category": "Arts & Crafts",
        "price": 89.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 45,
        "title": "Eco-Friendly Fashion Collection",
        "subtitle": "Stylish and Sustainable Apparel",
        "category": "Fashion",
        "price": 69.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 46,
        "title": "Luxury Bedside Table Set",
        "subtitle": "Elegant Nightstand Essentials",
        "category": "Home & Living",
        "price": 149.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 47,
        "title": "Home Office Ergonomic Accessories",
        "subtitle": "Enhance Comfort and Productivity",
        "category": "Electronics",
        "price": 49.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 48,
        "title": "Trail Running Gear Set",
        "subtitle": "Conquer Challenging Terrain",
        "category": "Sports & Outdoors",
        "price": 79.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 49,
        "title": "Asian Fusion Cuisine Cookbook",
        "subtitle": "Explore Harmonious Flavors",
        "category": "Food & Cooking",
        "price": 34.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
    {
        "id": 50,
        "title": "Handwoven Textile Art",
        "subtitle": "Unique Fabric Creations",
        "category": "Arts & Crafts",
        "price": 64.99,
        "photo": "https://source.unsplash.com/360x200?coffee"
    },
];

module.exports = products;

const express = require('express');
const router = express.Router();
const usersData = require('./users');
const productsData = require('./products');


router.get('/', (req, res) => {
    console.log('Entering / route');

    res.send(`
    <html>
    <head>
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
        <style>
            body {
                display: flex;
                font-family: Inter;
                align-items: center;
                justify-content: center;
                height: 100vh;
                margin: 0;
            }
            .container {
                text-align: center;
            }
            .primary-btn {
                background-color: #000;
                color: #fff;
                border: none;
                padding: 10px 20px;
                margin: 5px;
                cursor: pointer;
                border-radius: 5px;
                transition: all 0.3s;
            }
            .primary-btn:hover {
                background-color: #fff;
                color: #000;
                border: 2px solid #000;
            }
            .secondary-btn {
                background-color: #fff;
                color: #000;
                border: 2px solid #000;
                padding: 10px 20px;
                margin: 5px;
                cursor: pointer;
                border-radius: 5px;
                transition: all 0.3s;
            }
            .secondary-btn:hover {
                background-color: #000;
                color: #fff;
            }
        </style>
    </head>
    <body>
        <div class="container">
        <h1 id="currentTime">"Lau riwuh banget si, keq ga punya tuhan aja?"
            <br>
        </h1>
        <button class="primary-btn" onclick="window.location.href='/users'">/users</button>
        <button class="secondary-btn" onclick="window.location.href='/products'">/products</button>
        <button class="primary-btn" onclick="window.location.href='/image'">/image</button>
        </div>

        <script>
            setInterval(function() {
                var currentTimeElement = document.getElementById('currentTime');
                var now = new Date();
                var hours = now.getHours().toString().padStart(2, '0');
                var minutes = now.getMinutes().toString().padStart(2, '0');
                var seconds = now.getSeconds().toString().padStart(2, '0');
                currentTimeElement.innerHTML = 
                '"Lau riweh banget si der, keq ga punya tuhan aja?"<br>' + hours + ':' + minutes + ':' + seconds;
            }, 1000);
        </script>
    </body>
</html>
    `);
});

router.get('/users', (req, res) => {
    console.log('Entering /users route');
    const page = parseInt(req.query.page) || 1;
    const limit = 10;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    let filteredUsers = usersData; // feature search

    // feature search
    if (req.query.search) {
        const searchTerm = req.query.search.toLowerCase();
        console.log("/users:", searchTerm);
        filteredUsers = usersData.filter(user =>
            user.first_name.toLowerCase().includes(searchTerm)
        );
    }


    const pageUsers = filteredUsers.slice(startIndex, endIndex); // feature search

    const responseUsers = {
        data: pageUsers,
        meta: {
            total: filteredUsers.length, // feature search
            current_page: page,
            limit: limit,
            total_pages: Math.ceil(filteredUsers.length / limit), // feature search
        },
    };

    res.json(responseUsers);
});

router.get('/products', (req, res) => {
    console.log('Entering /products route');
    const page = parseInt(req.query.page) || 1;
    const limit = 10;
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    let filteredProducts = productsData; // feature search

    // feature search
    if (req.query.search) {
        const searchTerm = req.query.search.toLowerCase();
        console.log("/products:", searchTerm);
        filteredProducts = productsData.filter(produk =>
            produk.title.toLowerCase().includes(searchTerm)
        );
    }

    const pageProducts = filteredProducts.slice(startIndex, endIndex); // feature search

    const responseProducts = {
        data: pageProducts,
        meta: {
            total: filteredProducts.length, // feature search
            current_page: page,
            limit: limit,
            total_pages: Math.ceil(filteredProducts.length / limit), // feature search
        },
    };
    res.json(responseProducts);
});

module.exports = router;

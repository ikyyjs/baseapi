const users = [
    {
        "id": 1,
        "email": "george.bluth@reqres.in",
        "first_name": "George",
        "last_name": "Bluth",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 2,
        "email": "emma.smith@reqres.in",
        "first_name": "Emma",
        "last_name": "Smith",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 3,
        "email": "john.doe@reqres.in",
        "first_name": "John",
        "last_name": "Doe",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 4,
        "email": "lisa.jones@reqres.in",
        "first_name": "Lisa",
        "last_name": "Jones",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 5,
        "email": "alexander.smith@reqres.in",
        "first_name": "Alexander",
        "last_name": "Smith",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 6,
        "email": "olivia.miller@reqres.in",
        "first_name": "Olivia",
        "last_name": "Miller",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 7,
        "email": "david.white@reqres.in",
        "first_name": "David",
        "last_name": "White",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 8,
        "email": "natalie.brown@reqres.in",
        "first_name": "Natalie",
        "last_name": "Brown",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 9,
        "email": "michael.martin@reqres.in",
        "first_name": "Michael",
        "last_name": "Martin",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 10,
        "email": "sophia.anderson@reqres.in",
        "first_name": "Sophia",
        "last_name": "Anderson",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 11,
        "email": "sephia.lestari@reqres.in",
        "first_name": "Soephia",
        "last_name": "Lestari",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 12,
        "email": "david.jhon@reqres.in",
        "first_name": "David",
        "last_name": "Jhon",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 13,
        "email": "susan.white@reqres.in",
        "first_name": "Susan",
        "last_name": "White",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 14,
        "email": "robert.morris@reqres.in",
        "first_name": "Robert",
        "last_name": "Morris",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 15,
        "email": "oliver.brown@reqres.in",
        "first_name": "Oliver",
        "last_name": "Brown",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 16,
        "email": "grace.wilson@reqres.in",
        "first_name": "Grace",
        "last_name": "Wilson",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 17,
        "email": "diana.rodriguez@reqres.in",
        "first_name": "Diana",
        "last_name": "Rodriguez",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 18,
        "email": "leonard.hughes@reqres.in",
        "first_name": "Leonard",
        "last_name": "Hughes",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 19,
        "email": "natalie.cooper@reqres.in",
        "first_name": "Natalie",
        "last_name": "Cooper",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 20,
        "email": "adam.miller@reqres.in",
        "first_name": "Adam",
        "last_name": "Miller",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 21,
        "email": "emma.jones@reqres.in",
        "first_name": "Emma",
        "last_name": "Jones",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 22,
        "email": "henry.roberts@reqres.in",
        "first_name": "Henry",
        "last_name": "Roberts",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 23,
        "email": "laura.hernandez@reqres.in",
        "first_name": "Laura",
        "last_name": "Hernandez",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 24,
        "email": "jason.smith@reqres.in",
        "first_name": "Jason",
        "last_name": "Smith",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 25,
        "email": "olivia.johnson@reqres.in",
        "first_name": "Olivia",
        "last_name": "Johnson",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 26,
        "email": "william.harris@reqres.in",
        "first_name": "William",
        "last_name": "Harris",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 27,
        "email": "elizabeth.turner@reqres.in",
        "first_name": "Elizabeth",
        "last_name": "Turner",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 28,
        "email": "matthew.anderson@reqres.in",
        "first_name": "Matthew",
        "last_name": "Anderson",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 29,
        "email": "sophie.martin@reqres.in",
        "first_name": "Sophie",
        "last_name": "Martin",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 30,
        "email": "daniel.brown@reqres.in",
        "first_name": "Daniel",
        "last_name": "Brown",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 31,
        "email": "brian.wilson@reqres.in",
        "first_name": "Brian",
        "last_name": "Wilson",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 32,
        "email": "sara.james@reqres.in",
        "first_name": "Sara",
        "last_name": "James",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 33,
        "email": "nathan.evans@reqres.in",
        "first_name": "Nathan",
        "last_name": "Evans",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 34,
        "email": "alice.smith@reqres.in",
        "first_name": "Alice",
        "last_name": "Smith",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 35,
        "email": "ethan.jones@reqres.in",
        "first_name": "Ethan",
        "last_name": "Jones",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 36,
        "email": "mia.hernandez@reqres.in",
        "first_name": "Mia",
        "last_name": "Hernandez",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 37,
        "email": "ryan.baker@reqres.in",
        "first_name": "Ryan",
        "last_name": "Baker",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 38,
        "email": "emily.taylor@reqres.in",
        "first_name": "Emily",
        "last_name": "Taylor",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 39,
        "email": "michael.rogers@reqres.in",
        "first_name": "Michael",
        "last_name": "Rogers",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 40,
        "email": "lily.wright@reqres.in",
        "first_name": "Lily",
        "last_name": "Wright",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 41,
        "email": "andrew.morris@reqres.in",
        "first_name": "Andrew",
        "last_name": "Morris",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 42,
        "email": "mia.green@reqres.in",
        "first_name": "Mia",
        "last_name": "Green",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 43,
        "email": "nathan.cooper@reqres.in",
        "first_name": "Nathan",
        "last_name": "Cooper",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 44,
        "email": "olivia.evans@reqres.in",
        "first_name": "Olivia",
        "last_name": "Evans",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 45,
        "email": "ethan.roberts@reqres.in",
        "first_name": "Ethan",
        "last_name": "Roberts",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 46,
        "email": "emma.johnson@reqres.in",
        "first_name": "Emma",
        "last_name": "Johnson",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 47,
        "email": "jack.hernandez@reqres.in",
        "first_name": "Jack",
        "last_name": "Hernandez",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 48,
        "email": "sophia.adams@reqres.in",
        "first_name": "Sophia",
        "last_name": "Adams",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 49,
        "email": "michael.taylor@reqres.in",
        "first_name": "Michael",
        "last_name": "Taylor",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
    {
        "id": 50,
        "email": "lily.martin@reqres.in",
        "first_name": "Lily",
        "last_name": "Martin",
        "avatar": "https://source.unsplash.com/360x200?face"
    },
];

module.exports = users;

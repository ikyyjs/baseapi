const express = require('express');
const routes = require('./routes');
const router = express.Router();

const app = express();
const port = process.env.PORT || 3000;

console.log('Starting server...');


app.use('/', routes);

console.log('Routes have been initialized.');


app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

module.exports = router;
